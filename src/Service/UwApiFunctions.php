<?php

namespace Drupal\uw_api\Service;

use Drupal\breakpoint\BreakpointManagerInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\File\FileUrlGenerator;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Routing\RequestContext;
use Drupal\path_alias\AliasManager;
use Drupal\rest\ResourceResponse;
use Drupal\node\Entity\Node;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class UwApiFunctions.
 *
 * UW api functions.
 *
 * @package Drupal\uw_api\Service
 */
class UwApiFunctions {

  // The url to the api.
  const APIURL = '/api/v3.0';

  /**
   * Entity type manager from core.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The file url generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected FileUrlGeneratorInterface $fileUrlGenerator;

  /**
   * The current path.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * The current request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The break point manager.
   *
   * @var \Drupal\breakpoint\BreakpointManagerInterface
   */
  protected BreakpointManagerInterface $breakpointManager;

  /**
   * The request context.
   *
   * @var \Drupal\Core\Routing\RequestContext
   */
  protected $requestContext;

  /**
   * The path alias manager.
   *
   * @var \Drupal\path_alias\AliasManager
   */
  protected $aliasManager;

  /**
   * Constructor for api events.
   *
   * @param Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param Drupal\Core\File\FileUrlGenerator $fileUrlGenerator
   *   The file url generator.
   * @param Drupal\Core\Path\CurrentPathStack $currentPath
   *   The current path.
   * @param Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   * @param Drupal\breakpoint\BreakpointManagerInterface $breakpointManager
   *   The break point manager.
   * @param \Drupal\Core\Routing\RequestContext $requestContext
   *   The request context.
   * @param \Drupal\path_alias\AliasManager $aliasManager
   *   The path alias manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    FileUrlGenerator $fileUrlGenerator,
    CurrentPathStack $currentPath,
    RequestStack $requestStack,
    BreakpointManagerInterface $breakpointManager,
    RequestContext $requestContext,
    AliasManager $aliasManager
  ) {

    $this->entityTypeManager = $entityTypeManager;
    $this->fileUrlGenerator = $fileUrlGenerator;
    $this->currentPath = $currentPath;
    $this->requestStack = $requestStack;
    $this->breakpointManager = $breakpointManager;
    $this->requestContext = $requestContext;
    $this->aliasManager = $aliasManager;
  }

  /**
   * Function to get the terms from the parameters.
   *
   * @param array $param
   *   The parameters.
   *
   * @return array
   *   The array of terms.
   */
  public function getTagsFromParameters(array $param): array {

    // Return at least empty array of terms.
    $terms = [];

    // Step through each tag parameter and get its
    // term info.
    foreach ($param as $field_name => $field_values) {

      // Get the field name based on the content type.
      switch ($field_name) {

        case 'blog':
          $field_name = 'field_uw_blog_tags';
          $vid = 'uw_vocab_blog_tags';
          break;

        case 'catalog':
          $field_name = 'field_uw_catalog_category';
          $vid = 'uw_vocab_catalog_categories';
          break;

        case 'event':
          $field_name = 'field_uw_event_tags';
          $vid = 'uw_tax_event_tags';
          break;

        case 'news':
          $field_name = 'field_uw_news_tags';
          $vid = 'uw_vocab_news_tags';
          break;

        case 'profile':
          $field_name = 'field_uw_ct_profile_type';
          $vid = 'uw_vocab_profile_type';
          break;
      }

      // If there is no field or vocab, return an empty
      // array so that we do not throw errors.
      if (!$field_name && !$vid) {
        return [];
      }

      // If not an array of field values, then ensure
      // that we make an array of values.
      if (!is_array($field_values)) {
        $field_values = [$field_values];
      }

      // Step through each of the field values and setup
      // the array of terms.
      foreach ($field_values as $field_value) {

        // Load the term based on the name.
        $term = current($this->entityTypeManager
          ->getStorage('taxonomy_term')
          ->loadByProperties(
            [
              'name' => $field_value,
              'vid' => $vid,
            ]
          )
        );

        // If there is a term, get its id, if not set
        // the id to 0 which will result on null value
        // when performing the query.
        if ($term) {
          $terms[$field_name][] = $term->id();
        }
        else {
          $terms[$field_name][] = 0;
        }
      }
    }

    return $terms;
  }

  /**
   * Function to get the image info.
   *
   * @param \Drupal\Core\Field\EntityReferenceFieldItemListInterface $fields
   *   Image field.
   * @param string $image_style
   *   The machine name of the responsive image style.
   * @param string $media_entity_field
   *   The machine name of the field on the media.
   *
   * @return array
   *   Array of info about the image.
   */
  public function getImageInfoResponsive(
    EntityReferenceFieldItemListInterface $fields,
    string $image_style = 'uw_ris_media',
    string $media_entity_field = 'field_media_image'
  ): array {

    // Return at least an empty array.
    $image_info = [];

    // If image field cardinality is 1, there is only one image.
    foreach ($fields as $field) {

      /** @var \Drupal\media\MediaInterface $media_entity */
      $media_entity = $field->entity;

      if ($media_entity) {

        /** @var \Drupal\file\FileInterface $file */
        $file = $media_entity->field_media_image->entity;

        // Get the uri from the file.
        $uri = $file->getFileUri();

        // Get the full http to the host.
        $host = $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost();

        // Load Responsive Image Style and mappings.
        $responsiveImageStyle = $this->entityTypeManager
          ->getStorage('responsive_image_style')
          ->load($image_style);

        // Load theme breakpoints.
        $breakpoint_group = $responsiveImageStyle->getBreakpointGroup();

        // Load all the breakppoints.
        $breakpoints = $this->breakpointManager->getBreakpointsByGroup($breakpoint_group);

        // Get the image style entity.
        $image_style_entity = $this->entityTypeManager
          ->getStorage('image_style')
          ->load($responsiveImageStyle->getFallbackImageStyle());

        // Set the fallback image with the host.
        $image_info['img_element'] = $host;
        $image_info['img_element'] .= $this->fileUrlGenerator
          ->transformRelative($image_style_entity->buildUrl($uri));

        // Get the alt for the image.
        $image_info['alt'] = $media_entity->$media_entity_field->alt;

        // Get the image style mappings in reverse order so that
        // the responsive image will show correctly.
        $image_style_mappings = array_reverse($responsiveImageStyle->getImageStyleMappings());

        // Loop through image style mappings starting from smallest to largest
        // and build media queries.
        foreach ($image_style_mappings as $image_style_mapping) {

          // Get path to image using image style.
          $image_style = $image_style_mapping['image_mapping'];

          // Get the url to the image style.
          $image_style_url = $this->entityTypeManager
            ->getStorage('image_style')
            ->load($image_style)
            ->buildUrl($uri);

          // If not the portrait style, get the variables.
          if ($image_style !== 'uw_is_portrait') {

            $image_info['sources'][] = [
              'srcset' => $host . $this->fileUrlGenerator->transformRelative($image_style_url),
              'media' => $breakpoints[$image_style_mapping['breakpoint_id']]->getMediaQuery(),
              'type' => $file->getMimeType(),
              'style' => $image_style,
            ];
          }
        }
      }
    }

    return $image_info;
  }

  /**
   * Function to get the image with a style.
   *
   * @param Drupal\node\Entity\Node $node
   *   The node.
   * @param string $field
   *   The image field.
   * @param string $image_style
   *   The image style.
   *
   * @return array
   *   Array of info about the styled image.
   */
  public function getImageStyledImage(
    Node $node,
    string $field,
    string $image_style
  ): array {

    // Have at least an empty array.
    $image = [];

    // Get the media object from the node.
    $media = $node->$field->entity;

    // If there are media ids, process them.
    if ($media) {

      // Load the file from the media object.
      $file = $media->field_media_image->entity;

      // Get the image urls both original and styled, as well
      // as the alt text of the image.
      $image['original_image'] = $this->fileUrlGenerator
        ->generateAbsoluteString($file->getFileUri());
      $image['styled_image'] = $this->entityTypeManager
        ->getStorage('image_style')
        ->load($image_style)
        ->buildUrl($file->getFileUri());
      $image['alt'] = $media->field_media_image->alt;
    }

    return $image;
  }

  /**
   * Function to get the uw endpoints.
   *
   * @return string[]
   *   Array of endpoints.
   */
  public function getEndpoints(): array {

    return _uw_api_get_endpoints();
  }

  /**
   * Function to get the tags.
   *
   * @param Drupal\Core\Field\EntityReferenceFieldItemListInterface $fields
   *   The taxonomy term fields.
   *
   * @return array
   *   The array of tags.
   */
  public function getTags(EntityReferenceFieldItemListInterface ...$fields): array {

    // At least return an empty array.
    $tags = [];

    // Step through each of the fields and get the info
    // about the term.
    foreach ($fields as $field) {

      // Get the label which is the field name minus the field_uw_
      // part and as well as the content type.  For example a field
      // with the name field_uw_catalog_category, would be reduced
      // to just category.
      $label = $field->getName();
      $label = str_replace(
        'field_uw_',
        '',
        $label
      );
      $label = str_replace(
        $this->getContentType() . '_',
        '',
        $label
      );

      // Step through each term and now add it to the
      // tags array using the generated label from above.
      foreach ($field as $term) {

        // Need this extra check, just in case a term is deleted, but
        // has not been removed from the tags list.
        if ($entity = $term->entity) {
          $tags[$label][] = $entity->label();
        }
      }
    }

    // If there is only one field, then return just the
    // first array.
    if (count($tags) > 0 && count($fields) === 1) {
      $tags = current($tags);
    }

    return $tags;
  }

  /**
   * Function to get the content type.
   *
   * @return string
   *   The content type.
   */
  public function getContentType(): string {

    // Get the current path.
    $current_path = $this->currentPath->getPath();

    // Break the path into parts based on url.
    $path = explode('/', $current_path);

    // Return the last part of the array, since the
    // url will be /api/v3.0/<content_type>, we can
    // just return the last part of the array.
    return end($path);
  }

  /**
   * Function to info about a single date.
   *
   * @param string $date
   *   The date.
   * @param bool $with_time_flag
   *   The flag to include time.
   *
   * @return array
   *   Array of info about the date.
   */
  public function getDate(
    string $date,
    bool $with_time_flag = FALSE
  ): array {

    // Get the date format based on the flag.
    if ($with_time_flag) {
      $date = date('Y-m-d g:ia', strtotime($date . ' UTC'));
    }
    else {
      $date = date('Y-m-d', strtotime($date));
    }

    return [
      'timestamp' => strtotime($date),
      'date' => $date,
    ];
  }

  /**
   * Function to get the errors on a bad request.
   *
   * @param string $field
   *   The parameter field.
   * @param string $type
   *   The type of bad request.
   *
   * @return array[]
   *   Array of errors.
   */
  public function getBadRequestErrors(
    string $field,
    string $type = 'exist'
  ): array {

    // Get the message based on the type of filtering.
    switch ($type) {

      case 'invalid_date':
        $detail = 'Invalid filtering.  The field `' . $field . '` has an invalid date.';
        break;

      case 'extra_elements':
        $detail = 'Invalid filtering.  The field ' . $field . ' has too many elements to filter on.';
        break;

      default:
        $detail = 'Invalid filtering.  The field `' . $field . '` is invalid or does not exist.';
        break;
    }

    // Set the errors.
    $errors['errors'] = [
      'title' => 'Bad request',
      'status' => '400',
      'detail' => $detail,
      'links' => [
        'via' => [
          'href' => $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost() . $_SERVER['REQUEST_URI'],
        ],
        'info' => [
          'href' => 'https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.1',
        ],
      ],
    ];

    return $errors;
  }

  /**
   * Function to get info about the link.
   *
   * @param Drupal\node\Entity\Node $node
   *   The node.
   * @param string $field_name
   *   The field name.
   *
   * @return array
   *   Array of info about the link.
   */
  public function getLinkInfo(
    Node $node,
    string $field_name
  ): array {

    // Return at least and empty array.
    $link = [];

    // Get the field value from the node.
    $content = $node->$field_name->getValue();

    // If there is a value, then get the info into correct format.
    if (count($content) > 0) {
      $link['url'] = $content[0]['uri'];
      $link['title'] = $content[0]['title'];
    }

    return $link;
  }

  /**
   * Function to get the link to the node api.
   *
   * @param string $content_type
   *   The content type.
   * @param int $nid
   *   The node id.
   *
   * @return string
   *   The url to the node api endpoint.
   */
  public function getSelfLink(
    string $content_type,
    int $nid
  ): string {

    // Start the url with the full relative path.
    $url = $this->requestContext->getCompleteBaseUrl();

    // Add the url to the api.
    $url .= self::APIURL;

    // Add the content type to the url.
    $url .= '/' . $content_type;

    // Add the nid to the url.
    $url .= '?nid=' . $nid;

    return $url;
  }

  /**
   * Function to get the full path from the node.
   *
   * @param \Drupal\node\Entity\Node $node
   *   The node object.
   *
   * @return string
   *   The full relative path.
   */
  public function getFullPathFromNode(Node $node): string {

    // For some reason, some nodes do not have field_path, so just ensuring
    // that we have a path and so it does not error out.
    if (!$node->field_path->value) {
      $path = $this->aliasManager->getAliasByPath('/node/' . $node->id());
    }
    else {
      $path = $node->field_path->value;
    }

    // Return the full relative path to the node.
    return $this->requestContext->getCompleteBaseUrl() . $path;
  }

  /**
   * Function to get the field name based on the content type.
   *
   * @param string $content_type
   *   The content type.
   * @param string $field
   *   The field to get.
   *
   * @return string
   *   The field name.
   */
  public function getFieldName(
    string $content_type,
    string $field
  ): string {

    return _uw_api_get_field_name($content_type, $field);
  }

  /**
   * Function to get the allowed parameters for the api.
   *
   * @param string $content_type
   *   The content type.
   * @param bool $return_keys
   *   Flag to return only the array keys.
   *
   * @return string[]
   *   Array of allowed parameters.
   */
  public function getAllowedParameters(
    string $content_type,
    bool $return_keys = FALSE
  ): array {

    return _uw_api_get_allowed_paramters($content_type, $return_keys);
  }

  /**
   * Function to get the content type endpoint name and machine name.
   *
   * @param bool $return_keys
   *   Flag to return just the array keys.
   *
   * @return string[]
   *   Array of content type endpoints and machine names.
   */
  public function getContentTypes(bool $return_keys = FALSE): array {

    return _uw_api_get_content_types($return_keys);
  }

  /**
   * Function to check if date is in correct format.
   *
   * @param array $dates
   *   The date.
   *
   * @return bool
   *   If the date is in the right format.
   */
  public function checkDates(array $dates): bool {

    // Step through each date and check for proper formatting.
    foreach ($dates as $date) {

      // Break the date on the space so we can check if has a time.
      $parts = explode(' ', $date);

      // If there is only one part, we are looking at a date only.
      // If there is more than one, we have one with a time.
      if (count($parts) == 1) {

        // Regular expression to look for YYYY-MM-DD.
        $regex = '/(19|20)\d\d[-](0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01])/';
      }
      else {

        // Regular expression for YYYY-MM-DD HH:MMam|pm.
        $regex = '/(19|20)\d\d[-](0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01])[ ]([2-9]|1[0-2]?)[:]([0-5]{1}[0-9]{1})[ap][m]/';
      }

      // If there is no match return false.
      if (!preg_match($regex, $date)) {
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * Function to get the corrected dates.
   *
   * @param array $dates
   *   Array of dates.
   *
   * @return array
   *   Array of corrected dates.
   */
  public function getCorrectedDates(array $dates): array {

    // Have at least an empty array to return.
    $corrected_dates = [];

    // Step through each of the dates and get the corrected date.
    foreach ($dates as $date) {

      // Explode the parameter on the space, since the date
      // will be date<space>time.
      $parts = explode(' ', $date);

      // If there is more than one part, it means that we have
      // a date and a time, and we can put into the correct
      // format with the timezone, using the gmdate function.
      // If not we need to put in the date and use the like
      // operator when doing the query.
      if (count($parts) > 1) {
        $corrected_dates[] = [
          'date' => date(
              'Y-m-d',
              strtotime($date)) . 'T' . gmdate('H:i:s', strtotime($date)
          ),
          'type' => '=',
        ];
      }
      else {
        $corrected_dates[] = [
          'date' => $date,
          'type' => 'like',
        ];
      }
    }

    return $corrected_dates;
  }

  /**
   * Function to get the response with caching.
   *
   * @param array $api
   *   The array of api config.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The response with caching.
   */
  public function getResourceWithCaching(array $api): ResourceResponse {

    // Get the response using the api config.
    $response = new ResourceResponse($api);

    // Create the render array for caching.
    $build = [
      '#cache' => [
        'contexts' => [
          'url.query_args',
          'url.path',
        ],
        'max-age' => 0,
      ],
    ];

    // Create the cache metabase using the render array from above.
    $cache_metadata = CacheableMetadata::createFromRenderArray($build);

    // Add the caching to the response.
    $response->addCacheableDependency($cache_metadata);

    // Return the response with caching.
    return $response;
  }

}
