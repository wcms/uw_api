<?php

namespace Drupal\uw_api\Service;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\Sql\Query;

/**
 * Class UwApiQuery.
 *
 * UW api query.
 *
 * @package Drupal\uw_api\Service
 */
class UwApiQuery {

  /**
   * Entity type manager from core.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * UW api.
   *
   * @var \Drupal\uw_api\Service\UwApiFunctions
   */
  protected UwApiFunctions $uwApiFunctions;

  /**
   * Constructor for api events.
   *
   * @param Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param Drupal\uw_api\Service\UwApiFunctions $uwApiFunctions
   *   The uw api functions.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    UwApiFunctions $uwApiFunctions
  ) {

    $this->entityTypeManager = $entityTypeManager;
    $this->uwApiFunctions = $uwApiFunctions;
  }

  /**
   * Function to get add the like parameters to the query.
   *
   * @param Drupal\Core\Entity\Query\Sql\Query $query
   *   The query.
   * @param array $param
   *   The parameter.
   * @param string $field_name
   *   The name of the field.
   * @param string $condition_type
   *   The query condition.
   * @param string $operator
   *   The query operator.
   * @param string $date_type
   *   The type of date (start, end, between).
   */
  public function addParametersToQuery(
    Query &$query,
    array $param,
    string $field_name,
    string $condition_type,
    string $operator,
    string $date_type = NULL
  ): void {

    // The or group condition.
    $orGroup = $query->orConditionGroup();

    // If there are more than one parameter, use the or condition
    // to add to the query.  If there is only one parameter then
    // just use the regular condition.
    if (count($param) > 1) {

      // Step through the parameters for the parameter and add
      // them to the "or" group.
      foreach ($param as $p) {

        // If this is an event, we need to first create the and
        // condition and then add to the "or" condition.
        if ($condition_type == 'event') {

          // The occurring dates.
          if ($date_type == 'occurring_date') {
            $andGroup = $query->andConditionGroup()
              ->condition(
                'field_uw_event_date.value',
                strtotime($p . ' 23:59:59'),
                '<='
              )
              ->condition(
                'field_uw_event_date.end_value',
                strtotime($p . ' 00:00:00'),
                '>='
              );
          }
          // The start and end dates.
          else {
            // The "and" group for "in between" dates.
            $andGroup = $query->andConditionGroup()
              ->condition(
                'field_uw_event_date.value',
                strtotime($p . ' 00:00:00'),
                '>='
              )
              ->condition(
                'field_uw_event_date.value',
                strtotime($p . ' 23:59:59'),
                '<='
              );
          }

          // The or condition group.
          $orGroup->condition($andGroup);

          // Add the "or" group.
          $query->condition($orGroup);
        }
        else {

          // Add the parameter to the or group condition.
          $orGroup->condition(
            $field_name,
            $this->getConditionFromParameter(
              $condition_type,
              $p
            ),
            $operator
          );
        }
      }

      // Add the "or" group to the query condition.
      $query->condition($orGroup);
    }
    else {

      // If this is an event, we need to add the special
      // condition for less than and equal too and greater
      // than an equal to the start and end dates.
      // If not an even, we can just add the parameter.
      if ($condition_type == 'event') {

        // The occurring dates.
        if ($date_type == 'occurring_date') {
          $query->condition(
            'field_uw_event_date.value',
            strtotime($param[0] . ' 23:59:59'),
            '<='
          );
          $query->condition(
              'field_uw_event_date.end_value',
              strtotime($param[0] . ' 00:00:00'),
              '>='
          );
        }
        // The start and end dates.
        else {
          $andGroup = $query->andConditionGroup()
            ->condition(
              $field_name,
              strtotime($param[0] . ' 00:00:00'),
              '>='
            )
            ->condition($field_name,
              strtotime($param[0] . ' 23:59:59'),
              '<='
            );
          // Add the "and" group.
          $query->condition($andGroup);
        }

      }
      else {

        // Add the regular condition for the parameter.
        $query->condition(
          $field_name,
          $this->getConditionFromParameter(
            $condition_type,
            $param[0]
          ),
          $operator
        );
      }
    }
  }

  /**
   * Function to get the condition based on the parameter.
   *
   * @param string $condition_type
   *   The condition type.
   * @param string $parameter
   *   The parameter.
   *
   * @return string|array
   *   The actual condition.
   */
  public function getConditionFromParameter(
    string $condition_type,
    string $parameter
  ): string|array {

    // Have at least empty string to return.
    $condition = '';

    // Get the condition based on the condition type.
    switch ($condition_type) {

      case 'event':
        $condition = [
          strtotime($parameter . ' 00:00:00'),
        ];
        break;

      case 'like':
        $condition = '%' . $parameter . '%';
        break;

      case '=':
        $condition = $parameter;
        break;
    }

    return $condition;
  }

  /**
   * Function to add the corrected dates to the query.
   *
   * @param Drupal\Core\Entity\Query\Sql\Query $query
   *   The actual query.
   * @param array $dates
   *   The array of corrected dates.
   * @param string $index
   *   The current index.
   */
  public function addCorrectedDatesToQuery(
    Query &$query,
    array $dates,
    string $index
  ): void {

    // If there are more than one date, use the or condition.
    // If only one date use the regular query condition.
    if (count($dates) > 1) {

      // The or group condition.
      $orGroup = $query->orConditionGroup();

      // Step through the parameters for the parameter and add
      // them to the or group.
      foreach ($dates as $date) {

        // Get the condition based on the type of date.
        if ($date['type'] == '=') {
          $condition = $date['date'];
        }
        else {
          $condition = $date['date'] . '%';
        }

        // Add the parameter to the or group condition.
        $orGroup->condition(
          $this->uwApiFunctions->getFieldName(
            $this->uwApiFunctions->getContentType(),
            $index
          ),
          $condition,
          $date['type']
        );
      }
    }
    else {

      // Get the condition based on the type of query.
      if ($dates[0]['type'] == 'like') {
        $condition = $dates[0]['date'] . '%';
      }
      else {
        $condition = $dates[0]['date'];
      }

      // Add the regular condition for the parameter.
      $query->condition(
        $this->uwApiFunctions->getFieldName(
          $this->uwApiFunctions->getContentType(),
          $index
        ),
        $condition,
        $dates[0]['type']
      );
    }
  }

  /**
   * Function to add path to the query.
   *
   * @param Drupal\Core\Entity\Query\Sql\Query $query
   *   The query.
   * @param array $param
   *   The array of parameters.
   * @param string $index
   *   The current index of parameters.
   */
  public function addPathToQuery(
    Query &$query,
    array $param,
    string $index
  ): void {

    // If this has more than one filter, add them using and or group.
    // If it only has one filter, add regular condition.
    if (count($param) > 1) {

      // The or group condition.
      $orGroup = $query->orConditionGroup();

      // Step through each of the parameters and add to the
      // or group and then the actual condition.
      foreach ($param as $p) {

        // Add to the or group condition.
        $orGroup->condition(
          'field_path',
          $p
        );
      }

      // Add the or group to the query condition.
      $query->condition($orGroup);
    }
    else {

      // Add the path to the query.
      $query->condition('field_path', $param);
    }
  }

  /**
   * Function to add the taxonomy terms to the query.
   *
   * @param Drupal\Core\Entity\Query\Sql\Query $query
   *   The query.
   * @param array $param
   *   The array of parameters.
   * @param string $index
   *   The current index of parameters.
   */
  public function addTaxonomyTermsToQuery(
    Query &$query,
    array $param,
    string $index
  ): void {

    // If this has more than one filter, add them using and or group.
    // If it only has one filter, add regular condition.
    if (count($param) > 1) {

      // The or group condition.
      $orGroup = $query->orConditionGroup();

      // Step through each of the parameters and add to the
      // or group and then the actual condition.
      foreach ($param as $p) {

        // Use term id as zero for empty results unless we find
        // a term that matches the parameter.
        $term_id = 0;

        // Get the term from the parameter.
        $term = current($this->entityTypeManager
          ->getStorage('taxonomy_term')
          ->loadByProperties(['name' => $p]));

        // If there is a term, add to query field set term id.
        if ($term) {
          $term_id = $term->id();
        }

        // Add to the or group condition.
        $orGroup->condition(
          $this->uwApiFunctions->getFieldName(
            $this->uwApiFunctions->getContentType(),
            $index
          ),
          $term_id
        );
      }

      // Add the or group to the query condition.
      $query->condition($orGroup);
    }
    else {

      // Use term id as zero for empty results unless we find
      // a term that matches the parameter.
      $term_id = 0;

      // Get the term from the parameter.
      $term = current($this->entityTypeManager
        ->getStorage('taxonomy_term')
        ->loadByProperties(['name' => $param[0]]));

      // If there is a term, add to query field set term id.
      if ($term) {
        $term_id = $term->id();
      }

      // Add the condition using the term id, either 0 or
      // the term id that we found.
      $query->condition(
        $this->uwApiFunctions->getFieldName(
          $this->uwApiFunctions->getContentType(),
          $index
        ),
        $term_id
      );
    }
  }

  /**
   * Function to add the author to the query.
   *
   * @param Drupal\Core\Entity\Query\Sql\Query $query
   *   The query.
   * @param array $param
   *   The array of parameters.
   * @param string $content_type
   *   The content type.
   * @param string $field_name
   *   The field to filter on.
   */
  public function addAuthorToQuery(
    Query &$query,
    array $param,
    string $content_type,
    string $field_name
  ): void {

    // If the content type is blog, get the special author field.
    if ($content_type == 'blog') {

      // If there is more than one author, add each separately.
      if (count($param) > 1) {

        // The or group condition.
        $orGroup = $query->orConditionGroup();

        // Step through each of the parameters and add to the
        // or group and then the actual condition.
        foreach ($param as $p) {

          // Check if the author string is a Drupal user,
          // so we know if we need to check the node author.
          if ($p == 'wcmsadmin') {

            // The "wcmsadmin" user won't load, but we know it's UID 1.
            $uid = '1';
          }
          else {

            // There doesn't seem to be a way to get a single UID,
            // so we use the function that returns an array.
            $uids = $this->getUids($p);

            // We only want the first match in the array.
            // If there are no matches, this will be null.
            $uid = reset($uids);
          }

          // If there is a user, then get the author info.
          if ($uid) {

            // Check for the string against the node author,
            // but only when there is nothing in author field.
            $node_author = $query->andConditionGroup()
              ->condition('uid', $uid)
              ->notExists('field_author');

            // Set up the combined query.
            $orGroup->condition($node_author);
          }
          else {
            $orGroup->condition('field_author', $p);
          }

          // Add the or group to the query condition.
          $query->condition($orGroup);
        }
      }
      else {

        // Check for the author being in the author field.
        $field_author = $query->andConditionGroup()
          ->condition('field_author', $param[0]);

        // Check if the author string is a Drupal user,
        // so we know if we need to check the node author.
        if ($param[0] == 'wcmsadmin') {

          // The "wcmsadmin" user won't load, but we know it's UID 1.
          $uid = '1';
        }
        else {

          // There doesn't seem to be a way to get a single UID,
          // so we use the function that returns an array.
          $uids = $this->getUids($param[0]);

          // We only want the first match in the array.
          // If there are no matches, this will be null.
          $uid = reset($uids);
        }

        // If there is a user, then get the author info.
        if ($uid) {

          // Check for the string against the node author,
          // but only when there is nothing in author field.
          $node_author = $query->andConditionGroup()
            ->condition('uid', $uid)
            ->notExists('field_author');

          // Set up the combined query.
          $group = $query->orConditionGroup()
            ->condition($node_author)
            ->condition($field_author);
        }
        else {

          // There were no matching users, so just check the field.
          $group = $field_author;
        }

        // Add the conditions to the query condition.
        $query->condition($group);
      }
    }

    // If this is a pub ref, then get the value.
    if ($content_type == 'publication-reference') {

      // If there is only one author name to search.
      if (count($param) == 1) {

        // The or group condition.
        $orGroup = $query->orConditionGroup();

        // Since the field name is author_, we need to get
        // only the field name (i.e. without the author_).
        $field_name = str_replace('author_', '', $field_name);

        // The query to get the authors.
        $authors = $this->entityTypeManager
          ->getStorage('bibcite_contributor')
          ->getQuery();

        // Add the author to the conditions.
        $authors->condition($field_name, $param[0], 'LIKE');

        // Get the results of the authors.
        $results = $authors->execute();

        // If there are authors, then add them to the query.
        if (count($results) > 0) {

          // If there are more than one authors, we need to use
          // the or group to add the author to the query.
          if (count($results) > 1) {

            // Step through the authors and add to the or group.
            foreach ($results as $result) {
              $orGroup->condition('author', $result);
            }

            // Add the or query to the query.
            $query->condition($orGroup);
          }
          // There is only one author, just add it to the query.
          else {
            $query->condition('author', current($results));
          }
        }
        // There are no authors, so just use zero so that it will produce
        // no results on the filter, we will never have a 0 in the author
        // fields, this should not cause a problem.
        else {
          $query->condition('author', 0);
        }
      }
      else {

        // The or group condition.
        $orGroup = $query->orConditionGroup();

        // Since the field name is author_, we need to get
        // only the field name (i.e. without the author_).
        $field_name = str_replace('author_', '', $field_name);

        foreach ($param as $p) {

          // The query to get the authors.
          $authors = $this->entityTypeManager
            ->getStorage('bibcite_contributor')
            ->getQuery();

          // Add the author to the conditions.
          $authors->condition($field_name, $p, 'LIKE');

          // Get the results of the authors.
          $results = $authors->execute();

          // If there are authors, then add them to the query.
          if (count($results) > 0) {

            // If there are more than one authors, we need to use
            // the or group to add the author to the query.
            if (count($results) > 1) {

              // Step through the authors and add to the or group.
              foreach ($results as $result) {
                $orGroup->condition('author', $result);
              }

              // Add the or query to the query.
              $query->condition($orGroup);
            }
            // There is only one author, just add it to the query.
            else {
              $orGroup->condition('author', current($results));
              $query->condition($orGroup);
            }
          }
          // There are no authors, so just use zero so that it will produce
          // no results on the filter, we will never have a 0 in the author
          // fields, this should not cause a problem.
          else {
            $query->condition('author', 0);
          }
        }
      }
    }
  }

  /**
   * Function to add the keywords to the query.
   *
   * @param Drupal\Core\Entity\Query\Sql\Query $query
   *   The query.
   * @param array $param
   *   The array of parameters.
   * @param string $field_name
   *   The field to filter on.
   */
  public function addKeywordsToQuery(
    Query &$query,
    array $param,
    string $field_name
  ): void {

    // The or group condition.
    $orGroup = $query->orConditionGroup();

    // If there is only one keyword name to search.
    if (count($param) == 1) {

      // The query to get the keywords.
      $keywords = $this->entityTypeManager
        ->getStorage('bibcite_keyword')
        ->getQuery();

      // Add the author to the conditions.
      $keywords->condition('name', $param[0]);

      // Get the results of the authors.
      $results = $keywords->execute();

      // If there are authors, then add them to the query.
      if (count($results) > 0) {

        // If there are more than one keyword, we need to use
        // the or group to add the keyword to the query.
        if (count($results) > 1) {

          // Step through the authors and add to the or group.
          foreach ($results as $result) {
            $orGroup->condition('keywords', $result);
          }

          // Add the or query to the query.
          $query->condition($orGroup);
        }
        // There is only one author, just add it to the query.
        else {
          $query->condition('keywords', current($results));
        }
      }
      // There are no keywords, so just use zero so that it will produce
      // no results on the filter, we will never have a 0 in the keywords
      // fields, this should not cause a problem.
      else {
        $query->condition('keywords', 0);
      }
    }
    else {

      // Step through each of the parameters and get the query.
      foreach ($param as $p) {

        // The query to get the keywords.
        $keywords = $this->entityTypeManager
          ->getStorage('bibcite_keyword')
          ->getQuery();

        // Add the author to the conditions.
        $keywords->condition('name', $p);

        // Get the results of the authors.
        $results = $keywords->execute();

        // If there are authors, then add them to the query.
        if (count($results) > 0) {

          // If there are more than one keyword, we need to use
          // the or group to add the keyword to the query.
          if (count($results) > 1) {

            // Step through the authors and add to the or group.
            foreach ($results as $result) {
              $orGroup->condition('keywords', $result);
            }

            // Add the or query to the query.
            $query->condition($orGroup);
          }
          // There is only one author, just add it to the query.
          else {

            // Set the or condition.
            $orGroup->condition('keywords', current($results));

            // Add the or query to the query.
            $query->condition($orGroup);
          }
        }
        // There are no keywords, so just use zero so that it will produce
        // no results on the filter, we will never have a 0 in the keywords
        // fields, this should not cause a problem.
        else {
          $query->condition('keywords', 0);
        }
      }
    }
  }

  /**
   * Function get user ids from a parameter.
   *
   * @param string $param
   *   The parameter.
   *
   * @return array
   *   Array of user ids.
   */
  public function getUids(string $param): array {

    // Break the parameter by space for first and last name.
    $user_info = explode(' ', $param);

    // The query for users.
    $user_query = $this->entityTypeManager
      ->getStorage('user')
      ->getQuery();

    // If there is something in the first element, we will
    // use it as the first name, so add the condition.
    if (isset($user_info[0])) {
      $user_query->condition(
        'field_uw_first_name',
        $user_info[0]
      );
    }

    // If there is something in the first element, we will
    // use it as the last name, so add the condition.
    if (isset($user_info[1])) {
      $user_query->condition(
        'field_uw_last_name',
        $user_info[1]
      );
    }

    // Get the user ids based on the parameter.
    return $user_query->execute();
  }

}
