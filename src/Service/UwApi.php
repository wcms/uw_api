<?php

namespace Drupal\uw_api\Service;

use Drupal\Core\Entity\Query\Sql\Query;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class UwApi.
 *
 * UW api endpoints.
 *
 * @package Drupal\uw_api\Service
 */
class UwApi {

  /**
   * Entity type manager from core.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The current request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * UW api query.
   *
   * @var \Drupal\uw_api\Service\UwApiQuery
   */
  protected UwApiQuery $uwApiQuery;

  /**
   * UW api functions.
   *
   * @var \Drupal\uw_api\Service\UwApiFunctions
   */
  protected UwApiFunctions $uwApiFunctions;

  /**
   * Constructor for api events.
   *
   * @param Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   * @param Drupal\uw_api\Service\UwApiQuery $uwApiQuery
   *   The uw api query.
   * @param Drupal\uw_api\Service\UwApiFunctions $uwApiFunctions
   *   The uw api functions.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    RequestStack $requestStack,
    UwApiQuery $uwApiQuery,
    UwApiFunctions $uwApiFunctions
  ) {

    $this->entityTypeManager = $entityTypeManager;
    $this->requestStack = $requestStack;
    $this->uwApiQuery = $uwApiQuery;
    $this->uwApiFunctions = $uwApiFunctions;
  }

  /**
   * Function to get the query for the node ids.
   *
   * @param string $bundle
   *   The bundle.
   * @param string $entity_type
   *   The entity type.
   *
   * @return array
   *   Array of node ids.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getNids(string $bundle, string $entity_type = 'node'): array {

    // The query to get the nodes.
    $query = $this->entityTypeManager
      ->getStorage($entity_type)
      ->getQuery();

    // If this is a node, add the bundle type.
    if ($entity_type === 'node') {

      // Add the condition of bundle.
      $query->condition('type', $bundle);
    }

    // Get the parameters from the request.
    $params = $this->requestStack->getCurrentRequest()->query->all();

    // Step through each of the parameters and check if there
    // are multiple values in the parameter and if so then
    // convert it to an array.
    foreach ($params as $index => $param) {

      // Reset the new params array so that we do not have
      // duplicates.
      $new_params = [];

      // Explode the value on a comma.
      if (is_array($param)) {
        $parts = $param;
      }
      else {
        $parts = explode(',', $param);
      }

      // If there is more than one value in the parts,
      // convert it to an array.
      // If not just use the value.
      if (count($parts) > 1) {

        // Step through each of the parts and add it to the array.
        foreach ($parts as $part) {
          $new_params[] = $part;
        }
      }
      else {
        $new_params[] = $parts[0];
      }

      // Set the parameter to the new parameter with array.
      $params[$index] = $new_params;
    }

    // Get the allowed parameters.
    $allowed_params = $this->uwApiFunctions->getAllowedParameters(
      $this->uwApiFunctions->getContentType(),
      TRUE
    );

    // If there are parameters, process them.
    if (isset($params) && count($params) > 0) {

      // Step through each parameter, and add conditions
      // if there are filters.
      foreach ($params as $index => $param) {

        // If there is a bad filter, set the errors.
        if (!in_array($index, $allowed_params, TRUE)) {
          return $this->uwApiFunctions->getBadRequestErrors($index);
        }

        // Add the conditions to the query, based on the parameter.
        switch ($index) {

          // The author filter.
          case 'author':
          case 'author_first_name':
          case 'author_last_name':

            // Add the author to the query.
            $this->uwApiQuery->addAuthorToQuery(
              $query,
              $param,
              $this->uwApiFunctions->getContentType(),
              $index
            );
            break;

          // The application deadline.
          case 'application_deadline':

            // Check the date and if there is, send the bad request.
            if (!$this->uwApiFunctions->checkDates($param)) {
              return $this->uwApiFunctions->getBadRequestErrors(
                $index,
                'invalid_date'
              );
            }

            // Get the corrected dates.
            $dates = $this->uwApiFunctions->getCorrectedDates($param);

            // Add the corrected dates to the query.
            $this->uwApiQuery->addCorrectedDatesToQuery(
              $query,
              $dates,
              $index
            );
            break;

          // The filters with like in it.
          case 'affiliation':
          case 'title_position_description':
          case 'title':
          case 'title_contains':
          case 'date':
          case 'date_posted_application_open':
          case 'email':
          case 'job_id':
          case 'status':
          case 'year':
          case 'nid':
          case 'id':

            // If this is a title or title contains, the field
            // name is title and need to get correct operator
            // and condition type, title is equals and title
            // contains is like.
            // If not we get the field name based on the content
            // type.
            if (
              $index == 'title' ||
              $index == 'title_contains'
            ) {

              // The field name is title.
              $field_name = 'title';

              // Get the operator and condition type based
              // on the index.
              if ($index == 'title_contains') {
                $condition_type = 'like';
                $operator = 'LIKE';
              }
              else {
                $condition_type = '=';
                $operator = '=';
              }
            }
            elseif (
              $index == 'nid' ||
              $index == 'id'
            ) {

              // Set the field name, condition type and operator for the
              // nid and id parameters.
              $field_name = $index;
              $condition_type = '=';
              $operator = '=';
            }
            else {

              // Get the field name, condition type and operator
              // based on the content type.
              $field_name = $this->uwApiFunctions->getFieldName(
                $this->uwApiFunctions->getContentType(),
                $index
              );
              $condition_type = '=';
              $operator = '=';
            }

            // Add the parameters to the query.
            $this->uwApiQuery->addParametersToQuery(
              $query,
              $param,
              $field_name,
              $condition_type,
              $operator
            );
            break;

          // The single tag filter.
          case 'audience':
          case 'catalog':
          case 'category':
          case 'employment_type':
          case 'faculty':
          case 'opportunity_type':
          case 'project_status':
          case 'rate_of_pay_type':
          case 'type':
          case 'topics':
          case 'who_can_use':

            // Add the taxonomy term to the query.
            $this->uwApiQuery->addTaxonomyTermsToQuery(
              $query,
              $param,
              $index
            );
            break;

          case 'keywords':
            $this->uwApiQuery->addKeywordsToQuery(
              $query,
              $param,
              $index
            );
            break;

          // The date with time filter.
          case 'end_date':
          case 'start_date':
          case 'occurring_date':

            // Check the date and if there is, send the bad request.
            if (!$this->uwApiFunctions->checkDates($param)) {
              return $this->uwApiFunctions->getBadRequestErrors(
                $index,
                'invalid_date'
              );
            }

            // Add the start and end dates.
            $this->addStartEndDate(
              $query,
              $param,
              $index
            );
            break;

          // The path filter.
          case 'path':
            $this->uwApiQuery->addPathToQuery(
              $query,
              $param,
              $index
            );
            break;

          // The tag filter.
          case 'tags':

            // Get the terms from the parameters.
            $content_type_parts = str_replace('uw_ct_', '', $bundle);
            $content_type_parts = explode('_', $content_type_parts);
            $content_type = $content_type_parts[0];
            $terms = $this->uwApiFunctions->getTagsFromParameters(
              [$content_type => $param]
            );

            // If there are terms then add them to the conditions.
            if (!empty($terms)) {

              // Step through each of the terms and add the
              // conditions to the query.
              foreach ($terms as $field_name => $field_values) {

                // Set the query condition.
                $query->condition($field_name, $field_values, 'IN');
              }
            }
            break;
        }
      }
    }

    // Add the condition of published.
    $query->condition('status', 1);

    // Set the access check.
    $query->accessCheck(FALSE);

    // Get the nids from the query.
    return $query->execute();
  }

  /**
   * Function to add the start and end dates.
   *
   * @param \Drupal\Core\Config\Entity\Query\Query $query
   *   The query.
   * @param array $param
   *   The array of parameters.
   * @param string $index
   *   The current index of parameters.
   */
  private function addStartEndDate(
    Query &$query,
    array $param,
    string $index
  ): void {

    // Get the condition based on the content type.
    switch ($this->uwApiFunctions->getContentType()) {

      // The start and end date for events.
      case 'event':

        // Add the parameters to the query.
        $this->uwApiQuery->addParametersToQuery(
          $query,
          $param,
          $this->uwApiFunctions->getFieldName(
            $this->uwApiFunctions->getContentType(),
            $index
          ),
          'event',
          '<=',
          $index
        );
        break;

      // The start and end date for opportunities.
      case 'opportunity':

        // Add the parameters to the query.
        $this->uwApiQuery->addParametersToQuery(
          $query,
          $param,
          $this->uwApiFunctions->getFieldName(
            $this->uwApiFunctions->getContentType(),
            $index
          ),
          '=',
          '='
        );
        break;
    }
  }

}
