<?php

// phpcs:disable
namespace Drupal\uw_api\Plugin\rest\resource;
// phpcs:enable

use Drupal\Core\Routing\RequestContext;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\uw_api\Service\UwApiFunctions;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a resource to get news.
 *
 * @RestResource(
 *   id = "uw_api_listing",
 *   label = @Translation("UW API"),
 *   uri_paths = {
 *     "canonical" = "/api/v3.0"
 *   }
 * )
 */
class UwApiListing extends ResourceBase {

  // The url to the api.
  const APIURL = '/api/v3.0';

  /**
   * The current request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * UW api functions.
   *
   * @var \Drupal\uw_api\Service\UwApiFunctions
   */
  protected $uwApiFunctions;

  /**
   * The request context.
   *
   * @var \Drupal\Core\Routing\RequestContext
   */
  protected $requestContext;

  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   * @param \Drupal\uw_api\Service\UwApiFunctions $uwApiFunctions
   *   The uw api functions.
   * @param \Drupal\Core\Routing\RequestContext $requestContext
   *   The request context.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    RequestStack $requestStack,
    UwApiFunctions $uwApiFunctions,
    RequestContext $requestContext,
    array $serializer_formats,
    LoggerInterface $logger
  ) {

    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $serializer_formats,
      $logger
    );

    $this->requestStack = $requestStack;
    $this->uwApiFunctions = $uwApiFunctions;
    $this->requestContext = $requestContext;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('request_stack'),
      $container->get('uw_api.uw_api_functions'),
      $container->get('router.request_context'),
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest')
    );
  }

  /**
   * Get UW webpages endpoints.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   */
  public function get(): ResourceResponse {

    // At least have an empty data array to return.
    $data = [];

    // Get the list of endpoints.
    $endpoints = $this->uwApiFunctions->getEndpoints();

    // Step through each of the endpoints and get
    // the href to the actual endpoint.
    foreach ($endpoints as $endpoint) {
      $data['data'][$endpoint]['href'] = $this->requestContext->getCompleteBaseUrl() . self::APIURL . '/' . $endpoint;
    }

    return $this->uwApiFunctions->getResourceWithCaching($data);
  }

}
