<?php

// phpcs:disable
namespace Drupal\uw_api\Plugin\rest\resource;
// phpcs:enable

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\uw_api\Service\UwApi;
use Drupal\uw_api\Service\UwApiFunctions;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a resource to get publication references.
 *
 * @RestResource(
 *   id = "uw_api_publication_reference",
 *   label = @Translation("UW publication reference API"),
 *   uri_paths = {
 *     "canonical" = "/api/v3.0/publication-reference"
 *   }
 * )
 */
class UwApiPublicationReference extends ResourceBase {

  /**
   * UW api service.
   *
   * @var \Drupal\uw_api\Service\UwApi
   */
  protected $uwApi;

  /**
   * Entity type manager from core.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * UW api functions.
   *
   * @var \Drupal\uw_api\Service\UwApiFunctions
   */
  protected $uwApiFunctions;

  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\uw_api\Service\UwApi $uwApi
   *   The uw api service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\uw_api\Service\UwApiFunctions $uwApiFunctions
   *   The uw api functions.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    UwApi $uwApi,
    EntityTypeManagerInterface $entityTypeManager,
    UwApiFunctions $uwApiFunctions,
    array $serializer_formats,
    LoggerInterface $logger
  ) {

    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $serializer_formats,
      $logger
    );

    $this->uwApi = $uwApi;
    $this->entityTypeManager = $entityTypeManager;
    $this->uwApiFunctions = $uwApiFunctions;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('uw_api.uw_api'),
      $container->get('entity_type.manager'),
      $container->get('uw_api.uw_api_functions'),
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest')
    );
  }

  /**
   * Get UW publication reference endpoints.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   */
  public function get(): ResourceResponse {

    // At least have an empty data array to return.
    $data = [];

    $fields_not_to_include = [
      'bibcite_custom1',
      'bibcite_custom2',
      'bibcite_custom3',
      'bibcite_custom4',
      'bibcite_custom5',
      'bibcite_custom6',
      'bibcite_custom7',
      'uuid',
      'revision_id',
      'langcode',
      'revision_created',
      'revision_user',
      'revision_log_message',
      'uid',
      'status',
      'created',
      'changed',
      'revision_default',
      'moderation_state',
      'metatag',
      'path',
    ];

    $ids = $this->uwApi->getNids('', 'bibcite_reference');

    // If there are errors, set the api error message.
    // If there are no errors continue to process.
    if (isset($ids['errors'])) {
      $api = $ids['errors'];
    }
    else {

      // Step through each id and process it.
      foreach ($ids as $id) {

        // Load the entity.
        $entity = $this->entityTypeManager
          ->getStorage('bibcite_reference')
          ->load($id);

        // Get all the fields from the entity.
        $fields = $entity->getFields();

        // Step through each of the fields, and get the values.
        foreach ($fields as $field) {

          // Get the field name.
          $field_name = $field->getName();

          // If the field is not in the not include field list,
          // then process that field and get the value.
          if (!in_array($field->getName(), $fields_not_to_include)) {
            switch ($field_name) {

              // The author field.
              case 'author':
                $entity_data['author'] = $this->getAuthors($entity->$field_name->getValue());
                break;

              // The type field.
              case 'type':
                $entity_data[$field_name] = $entity->$field_name->getValue()[0]['target_id'];
                break;

              case 'keywords':
                $entity_data[$field_name] = $this->getKeywords($entity->$field_name->getValue());
                break;

              // All the rest of the fields will be field name with the value.
              default:
                // The default value is the field name but we want to remove the
                // bibcite_ prefix so it is more human readable.
                $entity_data[str_replace('bibcite_', '', $field_name)] = $entity->$field_name->value;
                break;
            }
          }
        }

        // Set the data.
        $data[] = $entity_data;
      }

      // Get the all the api data into one array.
      $api['data'] = $data;
      $api['meta']['count'] = count($ids);
      $api['filters'] = $this->uwApiFunctions->getAllowedParameters('publication-reference');
    }

    return $this->uwApiFunctions->getResourceWithCaching($api);
  }

  /**
   * Function to get all the authors.
   *
   * @param array $author_ids
   *   The info about the authors.
   *
   * @return array
   *   All the info about all the authors.
   */
  private function getAuthors(array $author_ids): array {

    // Return at least an empty array.
    $authors = [];

    // Step through each of the authors and get the info.
    foreach ($author_ids as $author_id) {

      // Load the author entity.
      $author_entity = $this->entityTypeManager
        ->getStorage('bibcite_contributor')
        ->load($author_id['target_id']);

      // Get the author info.
      $authors[] = [
        'category' => $author_id['category'],
        'first_name' => $author_entity->getFirstName(),
        'middle_name' => $author_entity->getMiddleName(),
        'last_name' => $author_entity->getLastName(),
      ];
    }

    return $authors;
  }

  /**
   * Function to get the keywords.
   *
   * @param array $keyword_ids
   *   The keyword ids.
   *
   * @return array
   *   All the info about the keywords.
   */
  private function getKeywords(array $keyword_ids): array {

    // Return at least an empty array.
    $keywords = [];

    // Step through each of the keyword ids and get the keyword.
    foreach ($keyword_ids as $keyword_id) {

      // Load the keyword entity.
      $keyword_entity = $this->entityTypeManager
        ->getStorage('bibcite_keyword')
        ->load($keyword_id['target_id']);

      // Add the keyword to the array.
      $keywords[] = $keyword_entity->getName();
    }

    return $keywords;
  }

}
