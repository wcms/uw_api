<?php

// phpcs:disable
namespace Drupal\uw_api\Plugin\rest\resource;
// phpcs:enable

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\node\Entity\Node;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\uw_api\Service\UwApi;
use Drupal\uw_api\Service\UwApiFunctions;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a resource to get projects.
 *
 * @RestResource(
 *   id = "uw_api_project",
 *   label = @Translation("UW project API"),
 *   uri_paths = {
 *     "canonical" = "/api/v3.0/project"
 *   }
 * )
 */
class UwApiProject extends ResourceBase {

  /**
   * UW api service.
   *
   * @var \Drupal\uw_api\Service\UwApi
   */
  protected $uwApi;

  /**
   * Entity type manager from core.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * UW api functions.
   *
   * @var \Drupal\uw_api\Service\UwApiFunctions
   */
  protected $uwApiFunctions;

  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\uw_api\Service\UwApi $uwApi
   *   The uw api service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\uw_api\Service\UwApiFunctions $uwApiFunctions
   *   The uw api functions.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    UwApi $uwApi,
    EntityTypeManagerInterface $entityTypeManager,
    UwApiFunctions $uwApiFunctions,
    array $serializer_formats,
    LoggerInterface $logger
  ) {

    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $serializer_formats,
      $logger
    );

    $this->uwApi = $uwApi;
    $this->entityTypeManager = $entityTypeManager;
    $this->uwApiFunctions = $uwApiFunctions;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('uw_api.uw_api'),
      $container->get('entity_type.manager'),
      $container->get('uw_api.uw_api_functions'),
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest')
    );
  }

  /**
   * Get UW profile endpoints.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   */
  public function get(): ResourceResponse {

    // At least have an empty data array to return.
    $data = [];

    // Get the nids from the query.
    $nids = $this->uwApi->getNids('uw_ct_project');

    // If there are errors, set the api error message.
    // If there are no errors continue to process.
    if (isset($nids['errors'])) {
      $api = $nids['errors'];
    }
    else {

      // Step through each nid and process it.
      foreach ($nids as $nid) {

        // Load the node.
        $node = $this->entityTypeManager
          ->getStorage('node')
          ->load($nid);

        // Get the data from the node.
        $node_data = [
          'nid' => $node->id(),
          'title' => $node->label(),
          'self' => [
            'href' => $this->uwApiFunctions->getSelfLink(
              $this->uwApiFunctions->getContentType(),
              $node->id()
            ),
          ],
          'listing_image' => $this->uwApiFunctions->getImageInfoResponsive(
            $node->field_uw_project_listing_image
          ),
          'path' => $this->uwApiFunctions->getFullPathFromNode($node),
          'project_members' => $this->getProjectMembers($node),
          'project_status' => $this->uwApiFunctions->getTags($node->field_uw_project_status),
          'summary' => $node->field_uw_project_summary->value,
          'timeline' => $this->getTimeline($node),
          'topics' => $this->uwApiFunctions->getTags($node->field_uw_project_topics),
          'audience' => $this->uwApiFunctions->getTags($node->field_uw_audience),
        ];

        // Set the data.
        $data[] = $node_data;
      }

      // Get the all the api data into one array.
      $api['data'] = $data;
      $api['meta']['count'] = count($nids);
      $api['filters'] = $this->uwApiFunctions->getAllowedParameters('project');
    }

    return $this->uwApiFunctions->getResourceWithCaching($api);
  }

  /**
   * Function to get the project members.
   *
   * @param \Drupal\node\Entity\Node $node
   *   The node.
   *
   * @return array
   *   Array of project members.
   */
  private function getProjectMembers(Node $node): array {

    // Have at least an empty array to return.
    $members = [];

    // Get the project members from the node.
    $project_members = $node->field_uw_project_members->getValue();

    // If there are project members, get the values.
    if (count($project_members) > 0) {

      // Counter to use for the members.
      $count = 0;

      // Step through each project members and get the values.
      foreach ($project_members as $project_member) {

        // Load the paragraph.
        $paragraph = $this->entityTypeManager
          ->getStorage('paragraph')
          ->load($project_member['target_id']);

        // Set the name for the project member.
        $members[$count]['name'] = $paragraph->field_uw_project_name->value;

        // Get the link for the project member.
        $link = $paragraph->field_uw_project_members_link->getValue();

        // If there is a link, get the value.
        if (count($link) > 0) {
          $members[$count]['link'] = $link[0]['uri'];
        }

        // Get the roles of the project member.
        $roles = $paragraph->field_uw_project_role->getValue();

        // If there are roles then get the values.
        if (count($roles) > 0) {

          // Step through each of the roles and get the values.
          foreach ($roles as $role) {

            // Load the taxonomy term.
            $term = $this->entityTypeManager
              ->getStorage('taxonomy_term')
              ->load($role['target_id']);

            // Add the term.
            $members[$count]['roles'][] = $term->label();
          }
        }

        // Increment the counter.
        $count++;
      }
    }

    return $members;
  }

  /**
   * Function to get the timeline.
   *
   * @param \Drupal\node\Entity\Node $node
   *   The node.
   *
   * @return array
   *   Array of timelines.
   */
  private function getTimeline(Node $node): array {

    // Reset the timeline and get the timeline from the node.
    $timeline = [];

    // Get the timeline from the node.
    $timeline_parts = $node->field_uw_project_timeline->getValue();

    // If there is a timeline, get the values.
    if (count($timeline_parts) > 0) {

      // Get the start and end time of the timeline.
      $timeline['start'] = date('Y-m-d', $timeline_parts[0]['value']);
      $timeline['end'] = date('Y-m-d', $timeline_parts[0]['end_value']);
    }

    return $timeline;
  }

}
