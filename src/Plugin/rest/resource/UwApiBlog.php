<?php

// phpcs:disable
namespace Drupal\uw_api\Plugin\rest\resource;
// phpcs:enable

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\uw_api\Service\UwApi;
use Drupal\uw_api\Service\UwApiFunctions;
use Drupal\uw_cfg_common\Service\UwNodeFieldValue;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a resource to get blogs.
 *
 * @RestResource(
 *   id = "uw_api_blog",
 *   label = @Translation("UW blog API"),
 *   uri_paths = {
 *     "canonical" = "/api/v3.0/blog"
 *   }
 * )
 */
class UwApiBlog extends ResourceBase {

  /**
   * UW api service.
   *
   * @var \Drupal\uw_api\Service\UwApi
   */
  protected UwApi $uwApi;

  /**
   * Entity type manager from core.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * UW Node field value.
   *
   * @var \Drupal\uw_cfg_common\Service\UwNodeFieldValue
   */
  protected UWNodeFieldValue $uwNodeFieldValue;

  /**
   * UW api functions.
   *
   * @var \Drupal\uw_api\Service\UwApiFunctions
   */
  protected UwApiFunctions $uwApiFunctions;

  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\uw_api\Service\UwApi $uwApi
   *   The uw api service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\uw_cfg_common\Service\UwNodeFieldValue $uwNodeFieldValue
   *   The node field value for UW.
   * @param Drupal\uw_api\Service\UwApiFunctions $uwApiFunctions
   *   The uw api functions.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    UwApi $uwApi,
    EntityTypeManagerInterface $entityTypeManager,
    UwNodeFieldValue $uwNodeFieldValue,
    UwApiFunctions $uwApiFunctions,
    array $serializer_formats,
    LoggerInterface $logger
  ) {

    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $serializer_formats,
      $logger
    );

    $this->uwApi = $uwApi;
    $this->entityTypeManager = $entityTypeManager;
    $this->uwNodeFieldValue = $uwNodeFieldValue;
    $this->uwApiFunctions = $uwApiFunctions;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('uw_api.uw_api'),
      $container->get('entity_type.manager'),
      $container->get('uw_cfg_common.uw_node_field_value'),
      $container->get('uw_api.uw_api_functions'),
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest')
    );
  }

  /**
   * Get UW blog endpoints.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function get(): ResourceResponse {

    // At least have an empty data array to return.
    $data = [];

    // Get the nids from the query.
    $nids = $this->uwApi->getNids('uw_ct_blog');

    // If there are errors, set the api error message.
    // If there are no errors continue to process.
    if (isset($nids['errors'])) {
      $api = $nids['errors'];
    }
    else {

      // Step through each nid and process it.
      foreach ($nids as $nid) {

        // Load the node.
        $node = $this->entityTypeManager
          ->getStorage('node')
          ->load($nid);

        // Get the data from the node.
        $node_data = [
          'nid' => $node->id(),
          'title' => $node->label(),
          'self' => [
            'href' => $this->uwApiFunctions->getSelfLink(
              $this->uwApiFunctions->getContentType(),
              $node->id()
            ),
          ],
          'author' => $node->field_author->value ?? $this->uwNodeFieldValue->getAuthor($node)['name'],
          'date' => $this->uwApiFunctions->getDate($node->field_uw_blog_date->value),
          'listing_image' => $this->uwApiFunctions->getImageInfoResponsive(
            $node->field_uw_blog_listing_page_image
          ),
          'path' => $this->uwApiFunctions->getFullPathFromNode($node),
          'summary' => $node->field_uw_blog_summary->value,
          'audience' => $this->uwApiFunctions->getTags(
            $node->field_uw_audience
          ),
          'tags' => $this->uwApiFunctions->getTags(
            $node->field_uw_blog_tags
          ),
        ];

        // Set the data.
        $data[] = $node_data;
      }

      // Get the all the api data into one array.
      $api['data'] = $data;
      $api['meta']['count'] = count($nids);
      $api['filters'] = $this->uwApiFunctions->getAllowedParameters('blog');
    }

    return $this->uwApiFunctions->getResourceWithCaching($api);
  }

}
