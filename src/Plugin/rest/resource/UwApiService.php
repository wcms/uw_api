<?php

// phpcs:disable
namespace Drupal\uw_api\Plugin\rest\resource;
// phpcs:enable

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\uw_api\Service\UwApi;
use Drupal\uw_api\Service\UwApiFunctions;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a resource to get catalog items.
 *
 * @RestResource(
 *   id = "uw_api_service",
 *   label = @Translation("UW service API"),
 *   uri_paths = {
 *     "canonical" = "/api/v3.0/service"
 *   }
 * )
 */
class UwApiService extends ResourceBase {

  /**
   * UW api service.
   *
   * @var \Drupal\uw_api\Service\UwApi
   */
  protected $uwApi;

  /**
   * Entity type manager from core.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * UW api functions.
   *
   * @var \Drupal\uw_api\Service\UwApiFunctions
   */
  protected $uwApiFunctions;

  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\uw_api\Service\UwApi $uwApi
   *   The uw api service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\uw_api\Service\UwApiFunctions $uwApiFunctions
   *   The uw api functions.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    UwApi $uwApi,
    EntityTypeManagerInterface $entityTypeManager,
    UwApiFunctions $uwApiFunctions,
    array $serializer_formats,
    LoggerInterface $logger
  ) {

    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $serializer_formats,
      $logger
    );

    $this->uwApi = $uwApi;
    $this->entityTypeManager = $entityTypeManager;
    $this->uwApiFunctions = $uwApiFunctions;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('uw_api.uw_api'),
      $container->get('entity_type.manager'),
      $container->get('uw_api.uw_api_functions'),
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest')
    );
  }

  /**
   * Get UW service endpoints.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   */
  public function get(): ResourceResponse {

    // At least have an empty data array to return.
    $data = [];

    // Get the nids from the query.
    $nids = $this->uwApi->getNids('uw_ct_service');

    // If there are errors, set the api error message.
    // If there are no errors continue to process.
    if (isset($nids['errors'])) {
      $api = $nids['errors'];
    }
    else {

      // Step through each nid and process it.
      foreach ($nids as $nid) {

        // Load the node.
        $node = $this->entityTypeManager
          ->getStorage('node')
          ->load($nid);

        // Get the data from the node.
        $node_data = [
          'nid' => $node->id(),
          'title' => $node->label(),
          'self' => [
            'href' => $this->uwApiFunctions->getSelfLink(
              $this->uwApiFunctions->getContentType(),
              $node->id()
            ),
          ],
          'average_length_to_complete' => $node->field_uw_service_length->value,
          'category' => $this->uwApiFunctions->getTags(
            $node->field_uw_service_category
          ),
          'cost' => $node->field_uw_service_cost->value,
          'location' => $this->getLocation($node->field_uw_service_location->getValue()),
          'location_coordinates' => $this->getLocationCoordinates($node->field_uw_service_location_coord->getValue()),
          'map' => $this->getMapInfo($node->field_uw_service_map->getValue()),
          'minimum_notice' => $node->field_uw_service_notice->value,
          'path' => $this->uwApiFunctions->getFullPathFromNode($node),
          'request_service' => $node->field_uw_service_request->value,
          'notes' => $node->field_uw_service_hours_notes->value,
          'service_hours' => $this->getServiceHours($node->field_uw_service_hours->getValue()),
          'status' => $node->field_uw_service_status->value,
          'summary' => $node->field_uw_service_summary->value,
          'whats_available' => $node->field_uw_service_available->value,
          'who_can_use' => $this->uwApiFunctions->getTags(
            $node->field_uw_service_audience
          ),
        ];

        // Set the data.
        $data[] = $node_data;
      }

      // Get the all the api data into one array.
      $api['data'] = $data;
      $api['meta']['count'] = count($nids);
      $api['filters'] = $this->uwApiFunctions->getAllowedParameters('service');
    }

    return $this->uwApiFunctions->getResourceWithCaching($api);
  }

  /**
   * Function to get the service hours.
   *
   * @param array $shrs
   *   The service hours from the node.
   *
   * @return array
   *   The service hours values.
   */
  private function getServiceHours(array $shrs): array {

    // Have at least an empty array to return.
    $service_hours = [];

    // If there are service hours, process them.
    if (count($shrs) > 0) {

      // The service hours days mapped to values.
      $service_hours_days = [
        '1' => 'Sunday',
        '2' => 'Monday',
        '3' => 'Tuesday',
        '4' => 'Wednesday',
        '5' => 'Thursday',
        '6' => 'Friday',
        '7' => 'Saturday',
      ];

      // Step through each of the service hours and get
      // the values.
      foreach ($shrs as $shr) {

        // Set the exception hour flag and reset values array.
        $exp_day = FALSE;
        $sh = [];

        // If the day is less than or equal to 7, it is a
        // regular day service hours.  If not, this is an
        // exception day.
        if (
          $shr['day'] <= 7
        ) {

          // If there is start hours or it is an all day,
          // then get the values of the service hour.
          if ($shr['starthours'] > 0 || $shr['all_day']) {

            // Get the service hour values.
            $sh = [
              'day' => $service_hours_days[$shr['day']],
              'starthours' => $shr['starthours'],
              'endhours' => $shr['endhours'],
              'all_day' => $shr['all_day'],
            ];
          }
        }
        else {

          // Set the exception day flag.
          $exp_day = TRUE;

          // Get the values of the exception day.
          $sh = [
            'day' => [
              'timestamp' => $shr['day'],
              'date' => date('Y-m-d', $shr['day']),
            ],
            'starthours' => $shr['starthours'],
            'endhours' => $shr['endhours'],
            'all_day' => $shr['all_day'],
          ];
        }

        // If there is a service hour value, then add it to
        // the array of service hours.
        if (count($sh) > 0) {

          // Add the service hour based on type of service hour.
          if ($exp_day) {
            $service_hours['exceptions'][] = $sh;
          }
          else {
            $service_hours['regular_hours'][] = $sh;
          }
        }
      }
    }

    return $service_hours;
  }

  /**
   * Function to get the location info.
   *
   * @param array $content
   *   The field value from location.
   *
   * @return array
   *   Array of location info.
   */
  private function getLocation(array $content): array {

    // Return at least an empty array.
    $location = [];

    // If there is a value in the content,
    // get the actual value from the array.
    if (count($content) > 0) {
      $location = [
        'country' => $content[0]['country_code'],
        'province_state' => $content[0]['administrative_area'],
        'city' => $content[0]['locality'],
        'postal_zip_code' => $content[0]['postal_code'],
        'address' => $content[0]['address_line1'],
        'address_2' => $content[0]['address_line2'],
        'organization' => $content[0]['organization'],
      ];
    }

    return $location;
  }

  /**
   * Function to get the location coordinates.
   *
   * @param array $content
   *   The location coordinates content.
   *
   * @return array
   *   Array of info about the location coordinates.
   */
  private function getLocationCoordinates(array $content): array {

    // Return at least an empty array.
    $loc_coord = [];

    // If there is a value in location coordinates,
    // get the actual value from the array.
    if (count($content) > 0) {
      $loc_coord = [
        'longitude' => $content[0]['lon'],
        'latitude' => $content[0]['lat'],
      ];
    }

    return $loc_coord;
  }

  /**
   * Function to get info about the map.
   *
   * @param array $content
   *   The map content.
   *
   * @return array
   *   Array of info about the map.
   */
  private function getMapInfo(array $content): array {

    // Return at least an empty array.
    $map = [];

    // If there is a value in map,
    // get the actual value from the array.
    if (count($content) > 0) {
      $map = $content[0]['uri'];
    }

    return $map;
  }

}
