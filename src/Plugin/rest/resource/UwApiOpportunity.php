<?php

// phpcs:disable
namespace Drupal\uw_api\Plugin\rest\resource;
// phpcs:enable

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\uw_api\Service\UwApi;
use Drupal\uw_api\Service\UwApiFunctions;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a resource to get opportunities.
 *
 * @RestResource(
 *   id = "uw_api_opportunity",
 *   label = @Translation("UW opportunity API"),
 *   uri_paths = {
 *     "canonical" = "/api/v3.0/opportunity"
 *   }
 * )
 */
class UwApiOpportunity extends ResourceBase {

  /**
   * UW api service.
   *
   * @var \Drupal\uw_api\Service\UwApi
   */
  protected $uwApi;

  /**
   * Entity type manager from core.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * UW api functions.
   *
   * @var \Drupal\uw_api\Service\UwApiFunctions
   */
  protected $uwApiFunctions;

  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\uw_api\Service\UwApi $uwApi
   *   The uw api service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param Drupal\uw_api\Service\UwApiFunctions $uwApiFunctions
   *   The uw api functions.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    UwApi $uwApi,
    EntityTypeManagerInterface $entityTypeManager,
    UwApiFunctions $uwApiFunctions,
    array $serializer_formats,
    LoggerInterface $logger
  ) {

    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $serializer_formats,
      $logger
    );

    $this->uwApi = $uwApi;
    $this->entityTypeManager = $entityTypeManager;
    $this->uwApiFunctions = $uwApiFunctions;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('uw_api.uw_api'),
      $container->get('entity_type.manager'),
      $container->get('uw_api.uw_api_functions'),
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest')
    );
  }

  /**
   * Get UW webpages endpoints.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   */
  public function get(): ResourceResponse {

    // At least have an empty data array to return.
    $data = [];

    // Get the nids from the query.
    $nids = $this->uwApi->getNids('uw_ct_opportunity');

    // If there are errors, set the api error message.
    // If there are no errors continue to process.
    if (isset($nids['errors'])) {
      $api = $nids['errors'];
    }
    else {

      // Step through each nid and process it.
      foreach ($nids as $nid) {

        // Load the node.
        $node = $this->entityTypeManager
          ->getStorage('node')
          ->load($nid);

        // Get the data from the node.
        $node_data = [
          'nid' => $node->id(),
          'title' => $node->label(),
          'self' => [
            'href' => $this->uwApiFunctions->getSelfLink(
              $this->uwApiFunctions->getContentType(),
              $node->id()
            ),
          ],
          'additional_info' => $node->field_uw_opportunity_additional->getValue(),
          'application_deadline' => $node->field_uw_opportunity_deadline->value ? $this->uwApiFunctions->getDate($node->field_uw_opportunity_deadline->value, TRUE) : NULL,
          'date_posted_application_open' => $node->field_uw_opportunity_date->value ? $this->uwApiFunctions->getDate($node->field_uw_opportunity_date->value) : NULL,
          'end_date' => $node->field_uw_opportunity_end_date->value ? $this->uwApiFunctions->getDate($node->field_uw_opportunity_end_date->value) : NULL,
          'employment_type' => $this->uwApiFunctions->getTags(
            $node->field_uw_opportunity_employment
          ),
          'job_id' => $node->field_uw_opportunity_job_id->value,
          'link' => $node->field_uw_opportunity_link->getValue(),
          'number_of_positions' => $node->field_uw_opportunity_link->value,
          'opportunity_type' => $this->uwApiFunctions->getTags(
            $node->field_uw_opportunity_type
          ),
          'path' => $this->uwApiFunctions->getFullPathFromNode($node),
          'position_summary' => $node->field_uw_opportunity_position->value,
          'posted_by' => $node->field_uw_opportunity_post_by->value,
          'rate_of_pay' => $node->field_uw_opportunity_pay_rate->value,
          'rate_of_pay_type' => $this->uwApiFunctions->getTags(
            $node->field_uw_opportunity_pay_type
          ),
          'reports_to' => $node->field_uw_opportunity_report->value,
          'start_date' => $node->field_uw_opportunity_start_date->value ? $this->uwApiFunctions->getDate($node->field_uw_opportunity_start_date->value) : NULL,
        ];

        // Set the data.
        $data[] = $node_data;
      }

      // Get the all the api data into one array.
      $api['data'] = $data;
      $api['meta']['count'] = count($nids);
      $api['filters'] = $this->uwApiFunctions->getAllowedParameters('opportunity');
    }

    return $this->uwApiFunctions->getResourceWithCaching($api);
  }

}
