<?php

// phpcs:disable
namespace Drupal\uw_api\Plugin\rest\resource;
// phpcs:enable

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\node\Entity\Node;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\uw_api\Service\UwApi;
use Drupal\uw_api\Service\UwApiFunctions;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a resource to get events.
 *
 * @RestResource(
 *   id = "uw_api_event",
 *   label = @Translation("UW event API"),
 *   uri_paths = {
 *     "canonical" = "/api/v3.0/event"
 *   }
 * )
 */
class UwApiEvent extends ResourceBase {

  /**
   * UW api service.
   *
   * @var \Drupal\uw_api\Service\UwApi
   */
  protected UwApi $uwApi;

  /**
   * Entity type manager from core.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * UW api functions.
   *
   * @var \Drupal\uw_api\Service\UwApiFunctions
   */
  protected UwApiFunctions $uwApiFunctions;

  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\uw_api\Service\UwApi $uwApi
   *   The uw api service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\uw_api\Service\UwApiFunctions $uwApiFunctions
   *   The uw api functions.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    UwApi $uwApi,
    EntityTypeManagerInterface $entityTypeManager,
    UwApiFunctions $uwApiFunctions,
    array $serializer_formats,
    LoggerInterface $logger
  ) {

    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $serializer_formats,
      $logger
    );

    $this->uwApi = $uwApi;
    $this->entityTypeManager = $entityTypeManager;
    $this->uwApiFunctions = $uwApiFunctions;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('uw_api.uw_api'),
      $container->get('entity_type.manager'),
      $container->get('uw_api.uw_api_functions'),
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest')
    );
  }

  /**
   * Get UW blog endpoints.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function get(): ResourceResponse {

    // At least have an empty data array to return.
    $data = [];

    // Get the nids from the query.
    $nids = $this->uwApi->getNids('uw_ct_event');

    // If there are errors, set the api error message.
    // If there are no errors continue to process.
    if (isset($nids['errors'])) {
      $api = $nids['errors'];
    }
    else {

      // Step through each nid and process it.
      foreach ($nids as $nid) {

        // Load the node.
        $node = $this->entityTypeManager
          ->getStorage('node')
          ->load($nid);

        // Get the data from the node.
        $node_data = [
          'nid' => $node->id(),
          'title' => $node->label(),
          'self' => [
            'href' => $this->uwApiFunctions->getSelfLink(
              $this->uwApiFunctions->getContentType(),
              $node->id()
            ),
          ],
          'all_dates' => $this->getAllDates($node),
          'listing_image' => $this->uwApiFunctions->getImageInfoResponsive(
            $node->field_uw_event_listing_page_img
          ),
          'next_date' => $this->getNextDate($node),
          'path' => $this->uwApiFunctions->getFullPathFromNode($node),
          'summary' => $node->field_uw_event_summary->value,
          'audience' => $this->uwApiFunctions->getTags($node->field_uw_audience),
          'tags' => $this->uwApiFunctions->getTags($node->field_uw_event_tags),
          'type' => $this->uwApiFunctions->getTags($node->field_uw_event_type),
        ];

        // Set the data.
        $data[] = $node_data;
      }

      // Get the all the api data into one array.
      $api['data'] = $data;
      $api['meta']['count'] = count($nids);
      $api['filters'] = $this->uwApiFunctions->getAllowedParameters('event');
    }

    return $this->uwApiFunctions->getResourceWithCaching($api);
  }

  /**
   * Function to get the next date for the events.
   *
   * @param \Drupal\node\Entity\Node $node
   *   The node object.
   *
   * @return array
   *   Array of the next date info.
   */
  private function getNextDate(Node $node): array {

    // Have at least and empty array to return.
    $date = [];

    // Get the date field from the node.
    $date_field = $node->field_uw_event_date->getValue();

    // If there is only one date, we can just return it.
    // If there is more than one date, we need to find
    // the next date to return.
    if (count($date_field) == 1) {

      $date = [
        'start_timestamp' => $date_field[0]['value'],
        'end_timestamp' => $date_field[0]['end_value'],
        'duration' => $date_field[0]['duration'],
      ];
    }
    else {

      // Get the current timestamp.
      $current_timestamp = time();

      // Step through each of the dates and find
      // the next date.
      foreach ($date_field as $df) {

        // If the listed date is greater than today's date,
        // of the date is the same, then we are going to
        // use that date to show for the next date.
        if (
          date('Y-m-d', $df['value']) == date('Y-m-d', $current_timestamp) ||
          $df['value'] >= $current_timestamp
        ) {

          // Set info about date of the node.
          $date = [
            'start_timestamp' => $df['value'],
            'end_timestamp' => $df['end_value'],
            'duration' => $df['duration'],
          ];

          // Break out of the loop to save computational time.
          break;
        }
      }
    }

    // If there is no date, it means all dates have past,
    // so use the very last date.
    if (empty($date)) {

      // Get the very last date.
      $df = end($date_field);

      // Get the basic info about the date.
      $date = [
        'start_timestamp' => $df['value'],
        'end_timestamp' => $df['end_value'],
        'duration' => $df['duration'],
      ];
    }

    // Get the all info about the date.
    $date = $this->getDateInfo($date);

    return $date;
  }

  /**
   * Function to get the info about the date.
   *
   * @param array $date
   *   Array of the date.
   *
   * @return array
   *   All the info about the date.
   */
  private function getDateInfo(array $date): array {

    // If the duration is 24 hours we have a possible all
    // day event, so we need to check if it is all day.
    if ($date['duration'] == 1439) {

      // Get the start and end dates.
      $start = date('g:i a', $date['start_timestamp']);
      $end = date('g:i a', $date['end_timestamp']);

      // If the start and end dates are 12 am and pm,
      // we have an all day event, if not it is just
      // a 24-hour event, so use that time.
      if ($start == '12:00 am' && $end == '11:59 pm') {

        // Set the info about the listed date.
        $date['all_day'] = TRUE;
      }
      else {

        // Set the info about the listed date.
        $date['all_day'] = FALSE;
      }
    }

    // Add the start and end readable date.
    $date['start_date'] = date('Y-m-d', $date['start_timestamp']);
    $date['start_time'] = date('g:i a', $date['start_timestamp']);
    $date['end_date'] = date('Y-m-d', $date['end_timestamp']);
    $date['end_time'] = date('g:i a', $date['end_timestamp']);

    return $date;
  }

  /**
   * Function to get all the dates of an event.
   *
   * @param \Drupal\node\Entity\Node $node
   *   The node object.
   *
   * @return array
   *   Array of all the dates.
   */
  private function getAllDates(Node $node): array {

    // Have at least an empty array to return.
    $dates = [];

    // Get the date field from the node.
    $date_field = $node->field_uw_event_date->getValue();

    // Step through each of the dates and get the
    // info about that date.
    foreach ($date_field as $df) {

      // Basic info about the date.
      $date = [
        'start_timestamp' => $df['value'],
        'end_timestamp' => $df['end_value'],
        'duration' => $df['duration'],
      ];

      // Get all the info about the date.
      $dates[] = $this->getDateInfo($date);
    }

    return $dates;
  }

}
