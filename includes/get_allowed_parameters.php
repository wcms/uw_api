<?php

/**
 * @file
 * Helper file.
 */

/**
 * Function to get the allowed parameters.
 *
 * @param string $content_type
 *   The content type.
 * @param bool $return_keys
 *   Flag to return only the array keys.
 *
 * @return string[]
 *   Array of allowed parameters.
 */
function _uw_api_get_allowed_paramters(string $content_type, bool $return_keys = FALSE): array {

  // Have at least empty array to return.
  $allowed_params = [];

  // Get the allowed parameters based on the content type.
  switch ($content_type) {

    case 'publication-reference':
      $allowed_params = [
        'id' => 'The id of the publication reference',
        'title' => 'The title of the publication reference',
        'title_contains' => 'The title of the publication reference contains',
        'author_first_name' => 'First name of the author for the publication reference (only one author first name filtering is allowed, no multiples)',
        'author_last_name' => 'Last name of the author for the publication reference (only one author last name filtering is allowed, no multiples)',
        'keywords' => 'Keyword in the publication reference (only one keyword filtering is allowed, no multiples)',
        'year' => 'The year of the publication reference',
      ];
      break;

    case 'blog':
      $allowed_params = [
        'nid' => 'The node id of the blog',
        'title' => 'The title of the blog',
        'title_contains' => 'The title of the blog contains',
        'author' => 'The author of the blog',
        'date' => 'The posted date of the blog',
        'path' => 'The path to the blog',
        'audience' => 'The audience to the news item',
        'tags' => 'The tags of the blog',
      ];
      break;

    case 'catalog':
      $allowed_params = [
        'nid' => 'The node id of the catalog item',
        'title' => 'The title of the catalog item',
        'title_contains' => 'The title of the catalog item contains',
        'audience' => 'The audience of the catalog item',
        'catalog' => 'The catalog that the item belongs to',
        'category' => 'The category that the item belongs to',
        'faculty' => 'The faculty that the item belongs to',
        'path' => 'The path to the catalog item',
      ];
      break;

    case 'contact':
      $allowed_params = [
        'nid' => 'The node id of the contact',
        'title' => 'The title of the contact',
        'title_contains' => 'The title of the contact contains',
        'affiliation' => 'The affiliation for the contact',
        'email' => 'The email of the contact',
        'path' => 'The path to the contact',
        'title_position_description' => 'The title, position or description of the contact',
      ];
      break;

    case 'event':
      $allowed_params = [
        'nid' => 'The node id of the event',
        'title' => 'The title of the event',
        'title_contains' => 'The title of the event contains',
        'end_date' => 'The end of the event',
        'occurring_date' => 'The events that occur beween that date, including start and end dates.',
        'path' => 'The path to the event',
        'start_date' => 'The start of the event',
        'audience' => 'The audience for the event',
        'tags' => 'The tags of the event',
        'type' => 'The type of event',
      ];
      break;

    case 'news':
      $allowed_params = [
        'nid' => 'The node id of the news item',
        'title' => 'The title of the news item',
        'title_contains' => 'The title of the news contains',
        'date' => 'The date of the news item',
        'path' => 'The path to the news item',
        'audience' => 'The audience to the news item',
        'tags' => 'The tags of the news item',
      ];
      break;

    case 'opportunity':
      $allowed_params = [
        'nid' => 'The node id of the opportunity',
        'title' => 'The title of the opportunity',
        'title_contains' => 'The title of the opportunity contains',
        'application_deadline' => 'The deadline for the application of the opportunity',
        'date_posted_application_open' => 'The date posted or application open for the opportunity',
        'employment_type' => 'The type of employment for the opportunity',
        'end_date' => 'The end date for the opportunity',
        'job_id' => 'The job id of the opportunity',
        'opportunity_type' => 'The type of opportunity',
        'path' => 'The path to the opportunity',
        'rate_of_pay_type' => 'The rate of pay type of the opportunity',
        'start_date' => 'The start date of the opportunity',
      ];
      break;

    case 'profile':
      $allowed_params = [
        'nid' => 'The node id of the profile',
        'title' => 'The title of the profile',
        'title_contains' => 'The title of the profile contains',
        'affiliation' => 'The affiliation for the profile',
        'path' => 'The path to the profile',
        'tags' => 'The tags of the profile',
        'title_position_description' => 'The title, position or description of the profile',
      ];
      break;

    case 'project':
      $allowed_params = [
        'nid' => 'The node id of the project',
        'title' => 'The title of the project',
        'title_contains' => 'The title of the project contains',
        'path' => 'The path to the project',
        'project_status' => 'The status of the project',
        'topics' => 'The topics of the project',
        'audience' => 'The audience to the project',
      ];
      break;

    case 'service':
      $allowed_params = [
        'nid' => 'The node id of the service',
        'title' => 'The title of the service',
        'title_contains' => 'The title of the service contains',
        'category' => 'The category of the service',
        'path' => 'The path to the service',
        'status' => 'The status of the service',
        'who_can_use' => 'The who can use this service',
      ];
      break;

    case 'webpage':
      $allowed_params = [
        'nid' => 'The node id of the web page',
        'title' => 'The title of the web page',
        'title_contains' => 'The title of the webpage contains',
        'path' => 'The path to the web page',
      ];
      break;
  }

  // Return the array keys if requested, if not return
  // the full array of allowed parameters.
  if ($return_keys) {
    return array_keys($allowed_params);
  }
  else {
    return $allowed_params;
  }
}
