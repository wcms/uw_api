<?php

/**
 * @file
 * Helper file.
 */

/**
 * Function to get the content type endpoint name and machine name.
 *
 * @param bool $return_keys
 *   Flag to return just the array keys.
 *
 * @return string[]
 *   Array of content type endpoints and machine names.
 */
function _uw_api_get_content_types(bool $return_keys = FALSE): array {

  // The list of endpoints and machine names for our
  // content types.
  $endpoints = [
    'bibcite' => 'bibcite_reference',
    'blog' => 'uw_ct_blog',
    'contact' => 'uw_ct_contact',
    'catalog' => 'uw_ct_catalog_item',
    'event' => 'uw_ct_event',
    'news' => 'uw_ct_news_item',
    'opportunity' => 'uw_ct_opportunity',
    'profile' => 'uw_ct_profile',
    'project' => 'uw_ct_project',
    'webpage' => 'uw_ct_web_page',
  ];

  // Return array keys if requested, if not return full array.
  if ($return_keys) {
    return array_keys($endpoints);
  }
  else {
    return $endpoints;
  }
}
