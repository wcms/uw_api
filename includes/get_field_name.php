<?php

/**
 * @file
 * Helper file.
 */

/**
 * Function to get field name.
 *
 * @param string $content_type
 *   The content type.
 * @param string $field
 *   The field.
 *
 * @return string
 *   The field machine name.
 */
function _uw_api_get_field_name(string $content_type, string $field): string {

  // Return at least empty field name.
  $field_name = '';

  // Get the field based on the content type.
  switch ($content_type) {

    // Blog content type fields.
    case 'blog':

      // Get the machine name of the blog field.
      switch ($field) {

        // The catalog audience field.
        case 'audience':
          $field_name = 'field_uw_audience';
          break;

        // The date field.
        case 'date':
          $field_name = 'field_uw_blog_date';
          break;
      }
      break;

    // Contact content type fields.
    case 'contact':

      // Get the machine name of the contact field.
      switch ($field) {

        // The affiliation field.
        case 'affiliation':
          $field_name = 'field_uw_ct_contact_affiliation';
          break;

        // The email field.
        case 'email':
          $field_name = 'field_uw_ct_contact_email';
          break;

        case 'title_position_description':
          $field_name = 'field_uw_ct_contact_title';
          break;
      }
      break;

    // Catalog content type fields.
    case 'catalog':

      // Get the machine name of the catalog field.
      switch ($field) {

        // The catalog audience field.
        case 'audience':
          $field_name = 'field_uw_audience';
          break;

        // The catalog term field.
        case 'catalog':
          $field_name = 'field_uw_catalog_catalog';
          break;

        // The category field.
        case 'category':
          $field_name = 'field_uw_catalog_category';
          break;

        // The faulty term field.
        case 'faculty':
          $field_name = 'field_uw_catalog_faculty';
          break;
      }
      break;

    // Event content type fields.
    case 'event':

      // Get the machine name of the event field.
      switch ($field) {

        // The event audience field.
        case 'audience':
          $field_name = 'field_uw_audience';
          break;

        // The end date field.
        case 'end_date':
          $field_name = 'field_uw_event_date.end_value';
          break;

        // The start date field.
        case 'start_date':
          $field_name = 'field_uw_event_date.value';
          break;

        // The event type field.
        case 'type':
          $field_name = 'field_uw_event_type';
          break;
      }
      break;

    // News content type field.
    case 'news':

      // Get the machine name of the news item field.
      switch ($field) {

        // The news audience field.
        case 'audience':
          $field_name = 'field_uw_audience';
          break;

        // The date field.
        case 'date':
          $field_name = 'field_uw_news_date';
          break;
      }
      break;

    // Opportunity content type field.
    case 'opportunity':

      // Get the machine name of the opportunity field.
      switch ($field) {

        // The application deadline field.
        case 'application_deadline':
          $field_name = 'field_uw_opportunity_deadline.value';
          break;

        // The date posted/application open field.
        case 'date_posted_application_open':
          $field_name = 'field_uw_opportunity_date.value';
          break;

        // The employment type term field.
        case 'employment_type':
          $field_name = 'field_uw_opportunity_employment';
          break;

        // The end date field.
        case 'end_date':
          $field_name = 'field_uw_opportunity_end_date.value';
          break;

        // The job id field.
        case 'job_id':
          $field_name = 'field_uw_opportunity_job_id';
          break;

        // The opportunity type term field.
        case 'opportunity_type':
          $field_name = 'field_uw_opportunity_type';
          break;

        // The rate of pay term field.
        case 'rate_of_pay_type':
          $field_name = 'field_uw_opportunity_pay_type';
          break;

        // The start date field.
        case 'start_date':
          $field_name = 'field_uw_opportunity_start_date.value';
          break;
      }
      break;

    // Profile content type field.
    case 'profile':

      // Get the machine name of the profile field.
      switch ($field) {

        // The affiliation field.
        case 'affiliation':
          $field_name = 'field_uw_ct_profile_affiliation';
          break;

        // The title, position or description field.
        case 'title_position_description':
          $field_name = 'field_uw_ct_profile_title';
          break;
      }
      break;

    // Project content type field.
    case 'project':

      // Get the machine name of the profile field.
      switch ($field) {

        // The news audience field.
        case 'audience':
          $field_name = 'field_uw_audience';
          break;

        // The status field.
        case 'project_status':
          $field_name = 'field_uw_project_status';
          break;

        // The topics field.
        case 'topics':
          $field_name = 'field_uw_project_topics';
          break;
      }
      break;

    // Bibcite content type field.
    case 'publication-reference':

      switch ($field) {
        case 'year':
          $field_name = 'bibcite_year';
          break;
      }
      break;

    // Service content type field.
    case 'service':

      // Get the machine name of the service field.
      switch ($field) {

        // The catalog field.
        case 'category':
          $field_name = 'field_uw_service_category';
          break;

        // The service status filter.
        case 'status':
          $field_name = 'field_uw_service_status';
          break;

        // The who can use this service filter.
        case 'who_can_use':
          $field_name = 'field_uw_service_audience';
          break;
      }
      break;
  }

  return $field_name;
}
