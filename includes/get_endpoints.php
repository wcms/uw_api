<?php

/**
 * @file
 * Helper file.
 */

/**
 * Function to get the uw endpoints.
 *
 * @return string[]
 *   Array of endpoints.
 */
function _uw_api_get_endpoints(): array {

  return [
    'uw_ct_blog' => 'blog',
    'uw_ct_catalog_item' => 'catalog',
    'uw_ct_contact' => 'contact',
    'uw_ct_events' => 'event',
    'uw_ct_news_item' => 'news',
    'uw_ct_opportunity' => 'opportunity',
    'uw_ct_profile' => 'profile',
    'uw_ct_project' => 'project',
    'bibcite' => 'publication-reference',
    'uw_ct_service' => 'service',
    'uw_ct_web_page' => 'webpage',
  ];
}
